/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-07-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#![feature(rust_2018_preview)]

extern crate conker;
extern crate glfw;
extern crate rand;

use conker::gfx2d::core::gl;
use conker::gfx2d::core::prelude::*;
use conker::gfx2d::drawing::*;

use glfw::{Action, Context, Key};
use rand::Rng;

include!("data/mario.rs");
include!("data/tie_fighter.rs");
include!("data/camera.rs");

fn setup_2d_camera(width: usize, height: usize) {
    unsafe {
        gl::MatrixMode(gl::PROJECTION);
        gl::LoadIdentity();
        gl::Viewport(0,
                     0,
                     width as gl::types::GLsizei,
                     height as gl::types::GLsizei);
        gl::MatrixMode(gl::MODELVIEW);
        gl::LoadIdentity();
        gl::Ortho(0.0,
                  width as gl::types::GLdouble,
                  height as gl::types::GLdouble,
                  0.0,
                  1.0,
                  -1.0);
    }
}

fn setup_gl() {
    unsafe {
        gl::Enable(gl::TEXTURE_2D);
    }
}

pub fn draw_grid<T: Copy+PartialEq>(pixels: &mut PixelRef<T>,
                                    bg_colour: T,
                                    grid_colour: T,
                                    tile_size: usize) {
    let width = pixels.width();
    let height = pixels.height();
    fill(pixels, bg_colour, 0, 0, width, height);

    let cols = width / (tile_size + 1);
    let rows = height / (tile_size + 1);

    for row in 0..=rows {
        draw_hline(pixels,
                   grid_colour,
                   0,
                   (row * (tile_size + 1)) as isize,
                   width);
    }

    for col in 0..=cols {
        draw_vline(pixels,
                   grid_colour,
                   (col * (tile_size + 1)) as isize,
                   0,
                   height);
    }
}

pub fn create_tie_fighter() -> PB<Rgb565> {
    // The original data is in RGB888 format. Convert it to Rgb565
    let image_data = TIE_FIGHTER_IMAGE_DATA.chunks(3)
                                           .map(|triage| {
                                               Rgb565::new(triage[0] >> 3,
                                                           triage[1] >> 2,
                                                           triage[2] >> 3)
                                           })
                                           .collect::<Vec<_>>();

    let width = 17;
    let height = 17;

    let mut tile = PB::new(width, height);

    {
        tile.lock().as_mut().copy_from_slice(&image_data);
    }

    tile
}

pub struct Tileset {
    pub pb:          PB<Rgb565>,
    pub tiles:       usize,
    pub tile_width:  usize,
    pub tile_height: usize,
    pub border:      usize,
}

pub fn create_camera_sprites() -> Tileset {
    let image_data = CAMERA_IMAGE_DATA.chunks(3)
                                      .map(|rgba| {
                                          Rgb565::new(rgba[0] >> 3,
                                                      rgba[1] >> 2,
                                                      rgba[2] >> 3)
                                      })
                                      .collect::<Vec<_>>();

    let tile_width = 54;
    let tile_height = 38;
    let border = 1;
    let tiles = 8;
    let rows = 2;
    let cols = 4;

    let width = tile_width * cols + (cols + 1) * border;
    let height = tile_height * rows + (rows + 1) * border;

    let mut pb = PB::new(width, height);
    {
        pb.lock().as_mut().copy_from_slice(&image_data);
    }

    Tileset { pb,
              tiles,
              tile_width,
              tile_height,
              border }
}

impl Tileset {
    pub fn tile_rect(&self, index: usize) -> Rect {
        let cols = self.pb.width() / self.tile_width;

        let row = index / cols;
        let col = index % cols;

        let x = self.border + col * self.tile_width + col * self.border;
        let y = self.border + row * self.tile_height + row * self.border;
        let width = self.tile_width;
        let height = self.tile_height;

        Rect::new(x as isize, y as isize, width, height)
    }
}

pub fn create_mario() -> PB<Rgb565> {
    let image_data = MARIO_IMAGE_DATA.chunks(4)
                                     .map(|rgba| {
                                         Rgb565::new(rgba[0] >> 3,
                                                     rgba[1] >> 2,
                                                     rgba[2] >> 3)
                                     })
                                     .collect::<Vec<_>>();

    let width = 71;
    let height = 71;

    let mut tile = PB::new(width, height);
    {
        tile.lock().as_mut().copy_from_slice(&image_data);
    }

    tile
}

fn main() {
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    glfw.window_hint(glfw::WindowHint::Resizable(false));

    let (mut window, events) =
        glfw.create_window(800, 600, "Outpost", glfw::WindowMode::Windowed)
            .expect("Failed to create GLFW window.");

    window.set_key_polling(true);
    window.make_current();

    glfw.set_swap_interval(glfw::SwapInterval::Adaptive);

    gl::load_with(|s| {
        window.get_proc_address(s) as *const std::os::raw::c_void
    });

    setup_gl();

    let (framebuffer_width, framebuffer_height) = window.get_framebuffer_size();

    let mut sc = SC::<Rgb565>::new(framebuffer_width as usize,
                                   framebuffer_height as usize);

    // let mut mario = create_mario();
    // let mario_rect = mario.clip_rect();

    let mut camera_ts = create_camera_sprites();
    let mut current_camera_frame = 0;
    let camera_frames = camera_ts.tiles;

    let width = framebuffer_width as usize;
    let height = framebuffer_height as usize;

    // let clip_rect = Rect::new(100, 100, width - 200, height - 200);
    // sc.set_clip_rect(clip_rect);

    let mut rng = rand::thread_rng();
    let _magenta = Rgb565::new(0x1f, 0, 0x1f);
    let _sky_blue = Rgb565::new(135 >> 3, 206 >> 2, 235 >> 3);
    let black = Rgb565::new(0, 0, 0);

    let mut stars = Vec::<(isize, isize)>::new();

    for _ in 0..200 {
        stars.push((rng.gen_range(-(width as isize), width as isize),
                    rng.gen_range(-(height as isize), height as isize)));
    }

    for _ in 0..100 {
        stars.push((rng.gen_range(0, width as isize),
                    rng.gen_range(0, height as isize)));
    }

    while !window.should_close() {
        setup_2d_camera(framebuffer_width as usize,
                        framebuffer_height as usize);
        unsafe {
            gl::ClearColor(1.0, 1.0, 1.0, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT
                      | gl::DEPTH_BUFFER_BIT
                      | gl::STENCIL_BUFFER_BIT);
        }

        {
            let mut pixels = sc.lock();
            clear(&mut pixels, black);

            // -- Move stars

            for mut star in &mut stars {
                draw_pixel(&mut pixels,
                           Rgb565::new(0x1f, 0x3f, 0x1f),
                           star.0,
                           star.1);
                star.0 += 1;
                star.1 += 1;

                if star.0 >= width as isize || star.1 >= height as isize {
                    star.0 = rng.gen_range(-(width as isize), width as isize);
                    star.1 = rng.gen_range(-(height as isize), height as isize);
                }
            }

            // -- Draw rotating cameras

            let camera_frame_rect = camera_ts.tile_rect(current_camera_frame);
            let camera_pixels = camera_ts.pb.lock();
            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       100,
                       100,
                       0.5,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       150,
                       150,
                       1.0,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       200,
                       200,
                       2.0,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       250,
                       250,
                       4.0,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       400,
                       400,
                       6.0,
                       Some(black));

            // ---

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       400,
                       100,
                       0.5,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       450,
                       150,
                       1.0,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       550,
                       200,
                       2.0,
                       Some(black));

            blt_scaled(&camera_pixels,
                       camera_frame_rect,
                       &mut pixels,
                       650,
                       250,
                       4.0,
                       Some(black));

            current_camera_frame += 1;
            if current_camera_frame >= camera_frames {
                current_camera_frame = 0;
            }

            // let step = 4;
            // let line_colour = Rgb565::new(16, 32, 16);

            // for i in (0..width).step_by(step) {
            //     draw_vline(&mut pixels, line_colour, i as isize, 0, height);
            // }

            // for i in (0..height).step_by(step) {
            //     draw_hline(&mut pixels, line_colour, 0, i as isize, width);
            // }
        }

        sc.swap_buffers();

        window.swap_buffers();

        glfw.poll_events();
        for (_, event) in glfw::flush_messages(&events) {
            handle_window_event(&mut window, &event);
        }

        std::thread::sleep(std::time::Duration::from_millis(100));
    }
}

fn handle_window_event(window: &mut glfw::Window, event: &glfw::WindowEvent) {
    match event {
        glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
            window.set_should_close(true)
        },
        _ => {},
    }
}
