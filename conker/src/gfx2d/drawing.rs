/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          24-09-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::{PixelRef, Rect};

#[inline(always)]
fn set_pixel<T: Copy>(pixels: &mut PixelRef<T>, colour: T, x: isize, y: isize) {
    let width = pixels.width();
    let x = x as usize;
    let y = y as usize;
    pixels[x + y * width] = colour;
}

#[inline(always)]
fn get_pixel<T: Copy>(pixels: &PixelRef<T>, x: isize, y: isize) -> T {
    let x = x as usize;
    let y = y as usize;
    pixels[x + y * pixels.width()]
}

#[inline(always)]
pub fn draw_pixel<T: Copy>(pixels: &mut PixelRef<T>,
                           colour: T,
                           x: isize,
                           y: isize) {
    if pixels.clip_rect().contains_point(x, y) {
        set_pixel(pixels, colour, x, y);
    }
}

pub fn draw_hline<T: Copy>(pixels: &mut PixelRef<T>,
                           colour: T,
                           x: isize,
                           y: isize,
                           length: usize) {
    for x in x..x + (length as isize) {
        set_pixel(pixels, colour, x, y);
    }
}

pub fn draw_vline<T: Copy>(pixels: &mut PixelRef<T>,
                           colour: T,
                           x: isize,
                           y: isize,
                           length: usize) {
    for y in y..y + (length as isize) {
        set_pixel(pixels, colour, x, y);
    }
}

pub fn draw_line<T: Copy>(_pixels: &mut PixelRef<T>,
                          _colour: T,
                          _x0: isize,
                          _y0: isize,
                          _x1: isize,
                          _y1: isize) {
    // https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
    // https://www.cs.helsinki.fi/group/goa/mallinnus/lines/bresenh.html
}

pub fn fill<T: Copy>(pixels: &mut PixelRef<T>,
                     colour: T,
                     x: isize,
                     y: isize,
                     width: usize,
                     height: usize) {
    fill_rect(pixels, colour, Rect::new(x, y, width, height));
}

pub fn fill_rect<T: Copy>(pixels: &mut PixelRef<T>, colour: T, rect: Rect) {
    if let Some(ir) = rect.intersection_with(pixels.clip_rect()) {
        for y in ir.y()..ir.bottom() {
            for x in ir.x()..ir.right() {
                set_pixel(pixels, colour, x, y);
            }
        }
    }
}

pub fn clear<T: Copy>(pixels: &mut PixelRef<T>, colour: T) {
    for p in pixels.as_mut() {
        *p = colour;
    }
}

pub fn blt<T: Copy+PartialEq>(src: &PixelRef<T>,
                              src_rect: Rect,
                              dst: &mut PixelRef<T>,
                              dst_x: isize,
                              dst_y: isize,
                              colour_key: Option<T>) {
    // TODO: Check that src_rect is in bounds of src
    let dst_rect = Rect::new(dst_x, dst_y, src_rect.width(), src_rect.height());
    if let Some((src_rect, dst_rect)) =
        compute_blt_rects(src_rect, dst_rect, dst.clip_rect())
    {
        let ys = dst_rect.height() as isize;
        let xs = dst_rect.width() as isize;

        if let Some(colour_key) = colour_key {
            for dy in 0..ys {
                let src_y = src_rect.y() + dy;

                for dx in 0..xs {
                    let src_x = src_rect.x() + dx;
                    let pixel = get_pixel(src, src_x, src_y);

                    if pixel != colour_key {
                        set_pixel(dst,
                                  pixel,
                                  dst_rect.x() + dx,
                                  dst_rect.y() + dy);
                    }
                }
            }
        } else {
            for dy in 0..ys {
                let src_y = src_rect.y() + dy;

                for dx in 0..xs {
                    let src_x = src_rect.x() + dx;
                    let pixel = get_pixel(src, src_x, src_y);

                    set_pixel(dst, pixel, dst_rect.x() + dx, dst_rect.y() + dy);
                }
            }
        }
    }
}

pub fn blt_scaled<T: Copy+PartialEq>(src: &PixelRef<T>,
                                     src_rect: Rect,
                                     dst: &mut PixelRef<T>,
                                     dst_x: isize,
                                     dst_y: isize,
                                     scale: f32,
                                     colour_key: Option<T>) {
    let dst_rect = Rect::new(dst_x,
                             dst_y,
                             (src_rect.width() as f32 * scale) as usize,
                             (src_rect.height() as f32 * scale) as usize);
    if let Some((src_rect, dst_rect)) =
        compute_blt_rects(src_rect, dst_rect, dst.clip_rect())
    {
        let ys = dst_rect.height() as isize;
        let xs = dst_rect.width() as isize;

        if let Some(colour_key) = colour_key {
            for dy in 0..ys {
                let src_y = src_rect.y() + (dy as f32 / scale) as isize;

                for dx in 0..xs {
                    let src_x = src_rect.x() + (dx as f32 / scale) as isize;
                    let pixel = get_pixel(src, src_x, src_y);

                    if pixel != colour_key {
                        set_pixel(dst,
                                  pixel,
                                  dst_rect.x() + dx,
                                  dst_rect.y() + dy);
                    }
                }
            }
        } else {
            for dy in 0..ys {
                let src_y = src_rect.y() + (dy as f32 / scale) as isize;

                for dx in 0..xs {
                    let src_x = src_rect.x() + (dx as f32 / scale) as isize;
                    let pixel = get_pixel(src, src_x, src_y);

                    set_pixel(dst, pixel, dst_rect.x() + dx, dst_rect.y() + dy);
                }
            }
        }
    }
}

// TODO: Move to a separate module: clipper/clipping.
fn compute_blt_rects(src_rect: Rect,
                     dst_rect: Rect,
                     dst_clip_rect: Rect)
                     -> Option<(Rect, Rect)> {
    if let Some(ir) = dst_rect.intersection_with(dst_clip_rect) {
        let src_x_offset = ir.x() - dst_rect.x();
        let src_y_offset = ir.y() - dst_rect.y();
        let src_x = src_rect.x() + src_x_offset;
        let src_y = src_rect.y() + src_y_offset;

        Some((Rect::new(src_x, src_y, ir.width(), ir.height()), ir))
    } else {
        None
    }
}
