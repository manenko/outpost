/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          12-10-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::cmp;
use std::ops::Range;

// TODO: Propably the trait is useful outside of the rect module. Make it public
// and put it into its own module.
trait RangeExt<T: Ord> {
    fn intersects_with(&self, other: &Range<T>) -> bool;
    fn intersection_with(&self, other: &Range<T>) -> Option<Range<T>>;
}

impl<T: Copy+Ord> RangeExt<T> for Range<T> {
    fn intersects_with(&self, other: &Range<T>) -> bool {
        if self.is_empty() || other.is_empty() {
            return false;
        }

        if other.start >= self.end {
            // self.start  self.end    other.start  other.end
            //     |           |            |            |
            // ----x===========x------------x============x---
            return false;
        }

        if other.end <= self.start {
            // other.start  other.end    self.start  self.end
            //      |            |           |           |
            // -----x============x-----------x===========x---
            return false;
        }

        true
    }

    fn intersection_with(&self, other: &Range<T>) -> Option<Self> {
        if self.is_empty() || other.is_empty() {
            return None;
        }

        let start = cmp::max(self.start, other.start);
        let end = cmp::min(self.end, other.end);
        let intersection = start..end;

        if intersection.is_empty() {
            return None;
        }

        Some(intersection)
    }
}

#[cfg(test)]
mod range_ext_tests {
    use super::RangeExt;

    #[test]
    fn test_has_intersection() {
        assert_eq!(false, (1..5).intersects_with(&(10..15)));
        assert_eq!(false, (1..5).intersects_with(&(5..15)));
        assert_eq!(false, (10..15).intersects_with(&(1..5)));
        assert_eq!(false, (10..15).intersects_with(&(1..10)));

        assert_eq!(true, (10..15).intersects_with(&(1..11)));
        assert_eq!(true, (10..15).intersects_with(&(11..12)));
        assert_eq!(true, (10..15).intersects_with(&(14..15)));

        assert_eq!(true, (1..5).intersects_with(&(4..6)));
        assert_eq!(true, (1..5).intersects_with(&(2..3)));
        assert_eq!(true, (1..5).intersects_with(&(4..5)));
    }

    #[test]
    fn test_intersection_with() {
        assert_eq!(None, (1..5).intersection_with(&(10..15)));
        assert_eq!(None, (1..5).intersection_with(&(5..15)));
        assert_eq!(None, (10..15).intersection_with(&(1..5)));
        assert_eq!(None, (10..15).intersection_with(&(1..10)));

        assert_eq!(Some(10..11), (10..15).intersection_with(&(1..11)));
        assert_eq!(Some(11..12), (10..15).intersection_with(&(11..12)));
        assert_eq!(Some(14..15), (10..15).intersection_with(&(14..15)));

        assert_eq!(Some(4..5), (1..5).intersection_with(&(4..6)));
        assert_eq!(Some(2..3), (1..5).intersection_with(&(2..3)));
        assert_eq!(Some(4..5), (1..5).intersection_with(&(4..5)));

        assert_eq!(Some(4..5), (4..5).intersection_with(&(4..5)));
    }
}

/// A rectangle with the origin at upper-left.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct Rect {
    x:      isize,
    y:      isize,
    width:  usize,
    height: usize,
}

impl Rect {
    /// Creates a rectangle at the given origin and with the given size.
    pub fn new(x: isize, y: isize, width: usize, height: usize) -> Self {
        Rect { x,
               y,
               width,
               height }
    }

    /// Creates a rectangle with the given size and origin at (0, 0).
    pub fn with_size(width: usize, height: usize) -> Self {
        Rect::new(0, 0, width, height)
    }

    /// Creates an empty rectangle.
    pub fn empty() -> Self {
        Rect::new(0, 0, 0, 0)
    }

    /// Returns the x-coordinate of the rectangle's origin.
    pub fn x(&self) -> isize {
        self.x
    }

    /// Returns the y-coordinate of the rectangle's origin.
    pub fn y(&self) -> isize {
        self.y
    }

    /// Returns the width of the rectangle.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Returns the height of the rectangle.
    pub fn height(&self) -> usize {
        self.height
    }

    /// Returns the x-coordinate of the rectangle's bottom-right corner.
    pub fn right(&self) -> isize {
        self.x + self.width as isize
    }

    /// Returns the y-coordinate of the rectangle's bottom-right corner.
    pub fn bottom(&self) -> isize {
        self.y + self.height as isize
    }

    /// Changes the x-coordinate of the rectangle's origin.
    pub fn set_x(&mut self, x: isize) {
        self.x = x;
    }

    /// Changes the y-coordinate of the rectangle's origin.
    pub fn set_y(&mut self, y: isize) {
        self.y = y;
    }

    /// Changes the width of the rectangle.
    pub fn set_width(&mut self, width: usize) {
        self.width = width;
    }

    /// Changes the height of the rectangle.
    pub fn set_height(&mut self, height: usize) {
        self.height = height;
    }

    /// Changes the size of the rectangle to the given width and height.
    pub fn resize_to(&mut self, width: usize, height: usize) {
        self.width = width;
        self.height = height;
    }

    /// Moves the origin of the rectangle to the given coordinates.
    pub fn move_to(&mut self, x: isize, y: isize) {
        self.x = x;
        self.y = y;
    }

    /// Moves the origin of the rectangle by the given offset.
    pub fn move_by(&mut self, dx: isize, dy: isize) {
        self.x += dx;
        self.y += dy;
    }

    pub fn resized_to(&self, width: usize, height: usize) -> Self {
        Rect::new(self.x, self.y, width, height)
    }

    pub fn moved_to(&self, x: isize, y: isize) -> Self {
        Rect::new(x, y, self.width, self.height)
    }

    pub fn moved_by(&self, dx: isize, dy: isize) -> Self {
        Rect::new(self.x + dx, self.y + dy, self.width, self.height)
    }

    /// Checks whether the rectangle has no area.
    pub fn is_empty(&self) -> bool {
        self.width == 0 || self.height == 0
    }

    /// Checks whether the given point resides inside the rectangle. A point
    /// along the right or bottom edge is not considered to be inside the
    /// rectangle; this way, a 1-by-1 rectangle contains only a single point.
    pub fn contains_point(&self, x: isize, y: isize) -> bool {
        x >= self.x && x < self.right() && y >= self.y && y < self.bottom()
    }

    /// Checks whether two rectangles intersects.
    ///
    /// Rectangles that share an edge but don't actually overlap are not
    /// considered to intersect.
    pub fn intersects_with(&self, other: Rect) -> bool {
        if !(self.x..self.right()).intersects_with(&(other.x..other.right())) {
            return false;
        }

        if !(self.y..self.bottom()).intersects_with(&(other.y..other.bottom()))
        {
            return false;
        }

        true
    }

    /// Returns an intersection of two rectangles if any.
    ///
    /// Rectangles that share an edge but don't actually overlap are not
    /// considered to intersect.
    pub fn intersection_with(&self, other: Rect) -> Option<Rect> {
        let h_self = self.x..self.right();
        let h_other = other.x..other.right();
        if let Some(h) = h_self.intersection_with(&h_other) {
            let v_self = self.y..self.bottom();
            let v_other = other.y..other.bottom();
            if let Some(v) = v_self.intersection_with(&v_other) {
                let intersection = Rect::new(h.start,
                                             v.start,
                                             (h.end - h.start) as usize,
                                             (v.end - v.start) as usize);

                return Some(intersection);
            }
        }

        None
    }
}
