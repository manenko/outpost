/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::gl;
use crate::gfx2d::core::pb::opengl;
use crate::gfx2d::core::pb::opengl::texture::descriptors::TextureDataDescriptor;
use crate::gfx2d::core::PixelBuffer;
use std::marker::PhantomData;
use std::ptr::null;

pub struct Texture<T> {
    id:  gl::types::GLuint,
    _ct: PhantomData<T>,
}

impl<T> Texture<T> where T: TextureDataDescriptor
{
    pub fn new(width: usize, height: usize) -> Texture<T> {
        unsafe {
            let _ct = PhantomData;
            let mut id = 0;
            gl::GenTextures(1, &mut id);
            gl::BindTexture(gl::TEXTURE_2D, id);
            gl::TexParameteri(gl::TEXTURE_2D,
                              gl::TEXTURE_MIN_FILTER,
                              gl::NEAREST as i32);
            gl::TexParameteri(gl::TEXTURE_2D,
                              gl::TEXTURE_MAG_FILTER,
                              gl::NEAREST as i32);
            gl::TexParameteri(gl::TEXTURE_2D,
                              gl::TEXTURE_WRAP_S,
                              gl::CLAMP as i32);
            gl::TexParameteri(gl::TEXTURE_2D,
                              gl::TEXTURE_WRAP_T,
                              gl::CLAMP as i32);

            gl::TexImage2D(gl::TEXTURE_2D,
                           0,
                           T::PIXEL_INTERNAL_FORMAT as gl::types::GLint,
                           width as gl::types::GLsizei,
                           height as gl::types::GLsizei,
                           0,
                           T::PIXEL_FORMAT,
                           T::PIXEL_TYPE,
                           null());

            Texture { id,
                      _ct }
        }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.id);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, 0);
        }
    }

    pub fn populate_from_surface(&mut self, pb: &opengl::PixelBuffer<T>) {
        pb.bind();
        unsafe {
            gl::TexSubImage2D(gl::TEXTURE_2D,
                              0,
                              0,
                              0,
                              pb.width() as gl::types::GLsizei,
                              pb.height() as gl::types::GLsizei,
                              T::PIXEL_FORMAT,
                              T::PIXEL_TYPE,
                              null());
        }
        pb.unbind();
    }
}

impl<T> Drop for Texture<T> {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteTextures(1, &self.id);
        }
    }
}
