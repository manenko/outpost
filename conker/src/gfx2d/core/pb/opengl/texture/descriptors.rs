/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          25-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::colour::{Rgb565, Rgba8888};
use crate::gfx2d::core::gl;

/// Describes client and GPU pixel data for OpenGL textures.
pub trait TextureDataDescriptor {
    /// Describes how the texture shall be stored in the GPU.
    const PIXEL_INTERNAL_FORMAT: gl::types::GLenum;

    /// Determines the composition of each element in a pixel data on client side.
    const PIXEL_FORMAT: gl::types::GLenum;

    /// Describes the data type of a pixel data on client side.
    const PIXEL_TYPE: gl::types::GLenum;
}

impl TextureDataDescriptor for Rgb565 {
    /// Determines the composition of each element in a pixel data on client side.
    const PIXEL_FORMAT: gl::types::GLenum = gl::RGB;
    /// Describes how the texture shall be stored in the GPU.
    const PIXEL_INTERNAL_FORMAT: gl::types::GLenum = gl::RGB;
    /// Describes the data type of a pixel data on client side.
    const PIXEL_TYPE: gl::types::GLenum = gl::UNSIGNED_SHORT_5_6_5;
}

impl TextureDataDescriptor for Rgba8888 {
    /// Determines the composition of each element in a pixel data on client side.
    const PIXEL_FORMAT: gl::types::GLenum = gl::RGBA;
    /// Describes how the texture shall be stored in the GPU.
    const PIXEL_INTERNAL_FORMAT: gl::types::GLenum = gl::RGBA8;
    /// Describes the data type of a pixel data on client side.
    const PIXEL_TYPE: gl::types::GLenum = gl::UNSIGNED_INT_8_8_8_8;
    // 0xRR,0xGG,0xBB,0xAA on Intel, little-endian machine is
    // 0xAABBGGRR. GL_UNSIGNED_BYTE preserves the format of binary
    // blocks of data across machines, while GL_UNSIGNED_INT_8_8_8_8
    // preserves the format of literals like 0xRRGGBBAA.
}
