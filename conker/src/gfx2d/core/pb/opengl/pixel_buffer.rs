/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::gl;
use crate::gfx2d::core::{self, PixelRef, Rect};
use std::marker::PhantomData;
use std::mem::size_of;
use std::ptr::null;

pub struct PixelBuffer<T> {
    id:        u32,
    width:     usize,
    height:    usize,
    clip_rect: Rect,
    _ct:       PhantomData<T>,
}

impl<T> PixelBuffer<T> {
    pub fn new(width: usize, height: usize) -> Self {
        assert_ne!(width, 0);
        assert_ne!(height, 0);

        let mut id = 0;
        let _ct = PhantomData;
        let size = width * height * size_of::<T>();
        let clip_rect = Rect::with_size(width, height);

        unsafe {
            gl::GenBuffers(1, &mut id);
            gl::BindBuffer(gl::PIXEL_UNPACK_BUFFER, id);
            gl::BufferData(gl::PIXEL_UNPACK_BUFFER,
                           size as gl::types::GLsizeiptr,
                           null(),
                           gl::STREAM_DRAW);
            gl::BindBuffer(gl::PIXEL_UNPACK_BUFFER, 0);
        }

        PixelBuffer { id,
                      width,
                      height,
                      clip_rect,
                      _ct }
    }

    pub fn bind(&self) {
        unsafe {
            gl::BindBuffer(gl::PIXEL_UNPACK_BUFFER, self.id);
        }
    }

    pub fn unbind(&self) {
        unsafe {
            gl::BindBuffer(gl::PIXEL_UNPACK_BUFFER, 0);
        }
    }

    fn unlock(_id: u32) {
        unsafe {
            gl::UnmapBuffer(gl::PIXEL_UNPACK_BUFFER);
            gl::BindBuffer(gl::PIXEL_UNPACK_BUFFER, 0);
        }
    }
}

impl<T> core::PixelBuffer<T> for PixelBuffer<T> {
    fn width(&self) -> usize {
        self.width
    }

    fn height(&self) -> usize {
        self.height
    }

    fn lock(&mut self) -> PixelRef<T> {
        self.bind();

        let size = self.width * self.height * size_of::<T>();
        unsafe {
            gl::BufferData(gl::PIXEL_UNPACK_BUFFER,
                           size as gl::types::GLsizeiptr,
                           null(),
                           gl::STREAM_DRAW);

            let buffer = gl::MapBuffer(gl::PIXEL_UNPACK_BUFFER, gl::READ_WRITE);
            let id = self.id;
            let unlock = move || {
                PixelBuffer::<T>::unlock(id);
            };

            PixelRef::from_raw_parts(buffer as *mut T,
                                     self.width,
                                     self.height,
                                     self.clip_rect,
                                     Some(Box::new(unlock)))
        }
    }

    fn clip_rect(&self) -> Rect {
        self.clip_rect
    }

    fn set_clip_rect(&mut self, rect: Rect) {
        assert!(!rect.is_empty());

        assert!(rect.x() >= 0);
        assert!(rect.y() >= 0);
        assert!(rect.right() <= self.width as isize);
        assert!(rect.bottom() <= self.height as isize);

        self.clip_rect = rect;
    }
}

impl<T> Drop for PixelBuffer<T> {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteBuffers(1, &self.id);
        }
    }
}
