/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::pb::opengl;
use crate::gfx2d::core::pb::opengl::texture::descriptors::TextureDataDescriptor;
use crate::gfx2d::core::pb::opengl::texture::Texture;
use crate::gfx2d::core::{self, gl, PixelBuffer, PixelRef, Rect};

/// A series of [`PixelBuffer`]s used for smooth, tear-free animation in games
/// and video playback.
///
/// [`PixelBuffer`]: trait.PixelBuffer.html
pub struct SwapChain<T>
    where T: TextureDataDescriptor
{
    pixel_buffer: opengl::PixelBuffer<T>,
    texture:      Texture<T>,
}

impl<T> SwapChain<T> where T: TextureDataDescriptor
{
    pub fn new(width: usize, height: usize) -> Self {
        let pixel_buffer = opengl::PixelBuffer::new(width, height);
        let texture = Texture::new(width, height);

        SwapChain { pixel_buffer,
                    texture }
    }

    fn render(&self) {
        unsafe {
            gl::Color3f(1.0, 1.0, 1.0);
            gl::Begin(gl::QUADS);
            {
                gl::TexCoord2f(0.0, 0.0);
                gl::Vertex2f(0.0, 0.0);

                gl::TexCoord2f(1.0, 0.0);
                gl::Vertex2f(self.width() as gl::types::GLfloat, 0.0);

                gl::TexCoord2f(1.0, 1.0);
                gl::Vertex2f(self.width() as gl::types::GLfloat,
                             self.height() as gl::types::GLfloat);

                gl::TexCoord2f(0.0, 1.0);
                gl::Vertex2f(0.0, self.height() as gl::types::GLfloat);
            }
            gl::End();
        }
    }
}

impl<T> PixelBuffer<T> for SwapChain<T> where T: TextureDataDescriptor
{
    fn width(&self) -> usize {
        self.pixel_buffer.width()
    }

    fn height(&self) -> usize {
        self.pixel_buffer.height()
    }

    fn lock(&mut self) -> PixelRef<T> {
        self.pixel_buffer.lock()
    }

    fn clip_rect(&self) -> Rect {
        self.pixel_buffer.clip_rect()
    }

    fn set_clip_rect(&mut self, rect: Rect) {
        self.pixel_buffer.set_clip_rect(rect)
    }
}

impl<T> core::SwapChain<T> for SwapChain<T> where T: TextureDataDescriptor
{
    fn swap_buffers(&mut self) {
        self.texture.bind();
        self.texture.populate_from_surface(&self.pixel_buffer);
        self.render();
        self.texture.unbind();
    }
}
