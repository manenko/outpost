/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::{self, PixelRef, Rect};

/// A [`PixelBuffer`] allocated in system memory.
///
/// [`PixelBuffer`]: ../trait.PixelBuffer.html
pub struct PixelBuffer<T> {
    buf:       Vec<T>,
    clip_rect: Rect,
    width:     usize,
    height:    usize,
}

impl<T> PixelBuffer<T> where T: Default+Copy
{
    /// Creates a new pixel buffer in system memory using the given `width`
    /// and `height`.
    pub fn new(width: usize, height: usize) -> Self {
        assert_ne!(width, 0);
        assert_ne!(height, 0);

        let length = width * height;
        let mut buf = Vec::with_capacity(length);
        buf.resize(length, T::default());

        let clip_rect = Rect::with_size(width, height);
        PixelBuffer { buf,
                      clip_rect,
                      width,
                      height }
    }
}

impl<T> core::PixelBuffer<T> for PixelBuffer<T> {
    fn width(&self) -> usize {
        self.width
    }

    fn height(&self) -> usize {
        self.height
    }

    fn lock(&mut self) -> PixelRef<T> {
        PixelRef::from_slice(&mut self.buf,
                             self.width,
                             self.height,
                             self.clip_rect,
                             None)
    }

    fn clip_rect(&self) -> Rect {
        self.clip_rect
    }

    fn set_clip_rect(&mut self, rect: Rect) {
        assert!(!rect.is_empty());

        assert!(rect.x() >= 0);
        assert!(rect.y() >= 0);
        assert!(rect.right() <= self.width as isize);
        assert!(rect.bottom() <= self.height as isize);

        self.clip_rect = rect;
    }
}
