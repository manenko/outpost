/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          06-07-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// Represents a colour value in `XRGB1555` format.
///
/// A single colour value is 16-bit integer with 5 bits for red,
/// green, and blue. The remaining bit is ignored.
///
/// **Memory representation**
///
/// ```text
///     15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// | X0 | R4 | R3 | R2 | R1 | R0 | G4 | G3 | G2 | G1 | G0 | B4 | B3 | B2 | B1 | B0 |
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
///   i  |      red component     |     green component    |     blue component     |
///   g  |
///   n  |
///   o  |
///   r  |
///   e  |
///   d  |
/// ```
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Xrgb1555(u16);

impl Xrgb1555 {
    /// Creates a new colour in `XRGB1555` format using the given `red`,
    /// `green`, and `blue` components.
    ///
    /// **Important: Value truncation**
    ///
    /// The maximum value of a colour component is 0x1F<sub>16</sub>
    /// or 31<sub>10</sub>, because each component occupies 5
    /// bits. That is black colour (the minimum possible value) in
    /// this format is `Xrgb1555(0, 0, 0)` and white colour (the
    /// maximum possible value) is `Xrgb1555(31, 31, 31)`.
    ///
    /// The function truncates each colour component to 5 bits. For
    /// example, a value of 0xAB<sub>16</sub> (or 171<sub>10</sub>)
    /// becomes 0xB<sub>16</sub> (or 11<sub>10</sub>) and not
    /// 0x1F<sub>16</sub> (or 31<sub>10</sub>).
    #[inline(always)]
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Xrgb1555(Self::truncate(blue)
                 + (Self::truncate(green) << 5)
                 + (Self::truncate(red) << 10))
    }

    /// Gets red component.
    #[inline(always)]
    pub fn red(self) -> u8 {
        ((self.0 & 0x7c00) >> 10) as u8
    }

    /// Gets green component.
    #[inline(always)]
    pub fn green(self) -> u8 {
        ((self.0 & 0x3e0) >> 5) as u8
    }

    /// Gets blue component.
    #[inline(always)]
    pub fn blue(self) -> u8 {
        (self.0 & 0x1f) as u8
    }

    /// Changes red component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 5 bits. Refer
    /// to the [`new`] method documentation for details.
    ///
    /// [`new`]: struct.Xrgb1555.html#important-value-truncation
    #[inline(always)]
    pub fn set_red(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0x83ff) + (Self::truncate(value) << 10);

        self
    }

    /// Changes green component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 5 bits. Refer
    /// to the [`new`] method documentation for details.
    ///
    /// [`new`]: struct.Xrgb1555.html#important-value-truncation
    #[inline(always)]
    pub fn set_green(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xfc1f) + (Self::truncate(value) << 5);

        self
    }

    /// Changes blue component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 5 bits. Refer
    /// to the [`new`] method documentation for details.
    ///
    /// [`new`]: struct.Xrgb1555.html#important-value-truncation
    #[inline(always)]
    pub fn set_blue(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xffe0) + Self::truncate(value);

        self
    }

    /// Truncates the given `u8` value to 5 bits and then converts it
    /// to `u16`.
    #[inline(always)]
    fn truncate(value: u8) -> u16 {
        u16::from(value) & 0x1f
    }
}

impl AsRef<u16> for Xrgb1555 {
    #[inline(always)]
    fn as_ref(&self) -> &u16 {
        &self.0
    }
}

impl AsMut<u16> for Xrgb1555 {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut u16 {
        &mut self.0
    }
}

impl From<Xrgb1555> for u16 {
    #[inline(always)]
    fn from(colour: Xrgb1555) -> u16 {
        colour.0
    }
}

#[cfg(test)]
mod xrgb1555_tests {
    use super::Xrgb1555;

    #[test]
    fn it_can_be_constructed_from_u16() {
        assert_eq!(Xrgb1555(0x7c1f).as_ref(), &0x7c1f);
    }

    #[test]
    fn it_can_be_constructed_from_red_green_and_blue() {
        assert_eq!(Xrgb1555::new(0xff, 0xff, 0xff).as_ref(), &0x7fff);
        assert_eq!(Xrgb1555::new(0x00, 0x1f, 0x00).as_ref(), &0x03e0);
        assert_eq!(Xrgb1555::new(0x00, 0x00, 0x1f).as_ref(), &0x001f);
        assert_eq!(Xrgb1555::new(0x1f, 0x00, 0x00).as_ref(), &0x7c00);
        assert_eq!(Xrgb1555::new(0xff, 0x00, 0x00).as_ref(), &0x7c00);
        assert_eq!(Xrgb1555::new(0x00, 0x00, 0x00).as_ref(), &0x0000);
    }

    #[test]
    fn it_returns_correct_value_for_red() {
        assert_eq!(Xrgb1555::new(0xff, 0xff, 0xff).red(), 0x1f);
        assert_eq!(Xrgb1555::new(0x00, 0xff, 0xff).red(), 0x00);
        assert_eq!(Xrgb1555::new(0x13, 0xff, 0xff).red(), 0x13);
    }

    #[test]
    fn it_returns_correct_value_for_green() {
        assert_eq!(Xrgb1555::new(0xaf, 0x01, 0xcf).green(), 0x01);
        assert_eq!(Xrgb1555::new(0x1f, 0xff, 0xff).green(), 0x1f);
        assert_eq!(Xrgb1555::new(0x1f, 0x04, 0x00).green(), 0x04);
    }

    #[test]
    fn it_returns_correct_value_for_blue() {
        assert_eq!(Xrgb1555::new(0xaf, 0x01, 0x16).blue(), 0x16);
        assert_eq!(Xrgb1555::new(0x1f, 0xff, 0xff).blue(), 0x1f);
        assert_eq!(Xrgb1555::new(0x1f, 0x04, 0x00).blue(), 0x00);
    }

    #[test]
    fn it_correctly_changes_red() {
        let mut colour = Xrgb1555::new(0xaf, 0xff, 0xab);
        assert_eq!(colour.as_ref(), &0x3feb);

        colour.set_red(0x04);

        assert_eq!(colour.red(), 0x04);
        assert_eq!(colour.as_ref(), &0x13eb);
    }

    #[test]
    fn it_correctly_changes_green() {
        let mut colour = Xrgb1555::new(0x1f, 0xff, 0xaa);
        assert_eq!(colour.as_ref(), &0x7fea);

        colour.set_green(0xac);

        assert_eq!(colour.green(), 0x0c);
        assert_eq!(colour.as_ref(), &0x7d8a);
    }

    #[test]
    fn it_correctly_changes_blue() {
        let mut colour = Xrgb1555::new(0x1f, 0xff, 0xa7);
        assert_eq!(colour.as_ref(), &0x7fe7);

        colour.set_blue(0xf7);

        assert_eq!(colour.blue(), 0x17);
        assert_eq!(colour.as_ref(), &0x7ff7);
    }
}

/// Represents a colour value in `RGB565` format.
///
/// A single colour value is 16-bit integer with 5 bits for red,
/// 6 for green, and 5 for blue.
///
/// **Memory representation**
///
/// ```text
///     15   14   13   12   11   10    9    8    7    6    5    4    3    2    1    0
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// | R4 | R3 | R2 | R1 | R0 | G5 | G4 | G3 | G2 | G1 | G0 | B4 | B3 | B2 | B1 | B0 |
/// +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
/// |     red component      |       green component       |     blue component     |
/// ```
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Rgb565(u16);

impl Rgb565 {
    #[inline(always)]
    fn truncate5(value: u8) -> u16 {
        u16::from(value & 0x1f)
    }

    #[inline(always)]
    fn truncate6(value: u8) -> u16 {
        u16::from(value & 0x3f)
    }

    /// Creates a new colour in `RGB565` format using the given `red`,
    /// `green`, and `blue` components.
    ///
    /// The maximum value of red and blue in this format is
    /// 0x1f. Green occupies more space (6 bits) and its maximum
    /// value is 0x3f.
    ///
    /// **Important!** The function doesn't round values to the
    /// maximum value for the corresponding colour component. It
    /// **truncates** each colour component on a bit level:
    ///
    /// - Red   to 5 bits.
    /// - Green to 6 bits.
    /// - Blue  to 5 bits.
    ///
    /// For example:
    ///
    /// - Red/Blue 0xff (0b1111_1111) becomes 0x1f (0b1_1111)
    /// - Red/Blue 0xab (0b1010_1011) becomes 0xb  (0b0_1011)
    /// - Green    0x4f (0b0100_1111) becomes 0xf  (0b00_1111)
    /// - Green    0xff (0b1111_1111) becomes 0x3f (0b11_1111)
    /// - etc.
    #[inline(always)]
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Rgb565(Self::truncate5(blue)
               + (Self::truncate6(green) << 5)
               + (Self::truncate5(red) << 11))
    }

    /// Gets red component.
    #[inline(always)]
    pub fn red(self) -> u8 {
        ((self.0 & 0xf800) >> 11) as u8
    }

    /// Gets green component.
    #[inline(always)]
    pub fn green(self) -> u8 {
        ((self.0 & 0x7e0) >> 5) as u8
    }

    /// Gets blue component.
    #[inline(always)]
    pub fn blue(self) -> u8 {
        (self.0 & 0x1f) as u8
    }

    /// Changes red component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 5
    /// bits. Refer to the constructor documentation for details.
    #[inline(always)]
    pub fn set_red(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0x7ff) + (Self::truncate5(value) << 11);

        self
    }

    /// Changes green component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 6
    /// bits. Refer to the constructor documentation for details.
    #[inline(always)]
    pub fn set_green(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xf81f) + (Self::truncate6(value) << 5);

        self
    }

    /// Changes blue component to the given `value`.
    ///
    /// **Note:** The function truncates the `value` to 5
    /// bits. Refer to the constructor documentation for details.
    #[inline(always)]
    pub fn set_blue(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xffe0) + Self::truncate5(value);

        self
    }
}

impl AsRef<u16> for Rgb565 {
    #[inline(always)]
    fn as_ref(&self) -> &u16 {
        &self.0
    }
}

impl AsMut<u16> for Rgb565 {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut u16 {
        &mut self.0
    }
}

impl From<Rgb565> for u16 {
    #[inline(always)]
    fn from(colour: Rgb565) -> u16 {
        colour.0
    }
}

#[cfg(test)]
mod rgb565_tests {
    use super::Rgb565;

    #[test]
    fn it_can_be_constructed_from_u16() {
        assert_eq!(Rgb565(0xabc).as_ref(), &0xabc);
    }

    #[test]
    fn it_can_be_constructed_from_red_green_and_blue() {
        assert_eq!(Rgb565::new(0x1f, 0x3f, 0x1f).as_ref(), &0xffff);
        assert_eq!(Rgb565::new(0x00, 0x3f, 0x00).as_ref(), &0x07e0);
        assert_eq!(Rgb565::new(0x00, 0x00, 0x00).as_ref(), &0x0000);
    }

    #[test]
    fn it_returns_correct_value_for_red() {
        assert_eq!(Rgb565::new(0xff, 0x1f, 0x0f).red(), 0x1f);
        assert_eq!(Rgb565::new(0x00, 0xff, 0xff).red(), 0x00);
        assert_eq!(Rgb565::new(0x13, 0x12, 0x0c).red(), 0x13);
    }

    #[test]
    fn it_returns_correct_value_for_green() {
        assert_eq!(Rgb565::new(0xaf, 0x3f, 0xcf).green(), 0x3f);
        assert_eq!(Rgb565::new(0x1f, 0x5f, 0xff).green(), 0x1f);
        assert_eq!(Rgb565::new(0x1f, 0x6f, 0x00).green(), 0x2f);
    }

    #[test]
    fn it_returns_correct_value_for_blue() {
        assert_eq!(Rgb565::new(0xaf, 0x01, 0x16).blue(), 0x16);
        assert_eq!(Rgb565::new(0x1f, 0xff, 0xff).blue(), 0x1f);
        assert_eq!(Rgb565::new(0x1f, 0x04, 0x00).blue(), 0x00);
    }

    #[test]
    fn it_correctly_changes_red() {
        let mut colour = Rgb565::new(0xaf, 0xff, 0xab);
        assert_eq!(colour.as_ref(), &0x7feb);

        colour.set_red(0x04);

        assert_eq!(colour.red(), 0x04);
        assert_eq!(colour.as_ref(), &0x27eb);
    }

    #[test]
    fn it_correctly_changes_green() {
        let mut colour = Rgb565::new(0x1f, 0xff, 0xaa);
        assert_eq!(colour.as_ref(), &0xffea);

        colour.set_green(0xac);

        assert_eq!(colour.green(), 0x2c);
        assert_eq!(colour.as_ref(), &0xfd8a);
    }

    #[test]
    fn it_correctly_changes_blue() {
        let mut colour = Rgb565::new(0x1f, 0xff, 0xa7);
        assert_eq!(colour.as_ref(), &0xffe7);

        colour.set_blue(0xf7);

        assert_eq!(colour.blue(), 0x17);
        assert_eq!(colour.as_ref(), &0xfff7);
    }
}

/// Represents a colour value in `RGBX8888` format.
///
/// A single colour value is 32-bit integer with 8 bits for red,
/// green, and blue.
///
/// **Memory representation**
///
/// ```text
/// RRRRRRRR GGGGGGGG BBBBBBBB XXXXXXXX
/// ```
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Rgbx8888(u32);

impl Rgbx8888 {
    #[inline(always)]
    fn make_red(value: u8) -> u32 {
        u32::from(value) << 24
    }

    #[inline(always)]
    fn make_green(value: u8) -> u32 {
        u32::from(value) << 16
    }

    #[inline(always)]
    fn make_blue(value: u8) -> u32 {
        u32::from(value) << 8
    }

    /// Creates a new colour in `RGBX8888` format using the given `red`,
    /// `green`, and `blue` components.
    #[inline(always)]
    pub fn new(red: u8, green: u8, blue: u8) -> Self {
        Rgbx8888(Self::make_red(red)
                 + Self::make_green(green)
                 + Self::make_blue(blue))
    }

    /// Gets red component.
    #[inline(always)]
    pub fn red(self) -> u8 {
        ((self.0 & 0xff00_0000) >> 24) as u8
    }

    /// Gets green component.
    #[inline(always)]
    pub fn green(self) -> u8 {
        ((self.0 & 0x00ff_0000) >> 16) as u8
    }

    /// Gets blue component.
    #[inline(always)]
    pub fn blue(self) -> u8 {
        ((self.0 & 0x0000_ff00) >> 8) as u8
    }

    /// Changes red component to the given `value`.
    #[inline(always)]
    pub fn set_red(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0x00ff_ff00) + Self::make_red(value);

        self
    }

    /// Changes green component to the given `value`.
    #[inline(always)]
    pub fn set_green(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xff00_ff00) + Self::make_green(value);

        self
    }

    /// Changes blue component to the given `value`.
    #[inline(always)]
    pub fn set_blue(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xffff_0000) + Self::make_blue(value);

        self
    }
}

impl AsRef<u32> for Rgbx8888 {
    #[inline(always)]
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

impl AsMut<u32> for Rgbx8888 {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut u32 {
        &mut self.0
    }
}

impl From<Rgbx8888> for u32 {
    #[inline(always)]
    fn from(colour: Rgbx8888) -> u32 {
        colour.0
    }
}

/// Represents a colour value in `RGBA8888` format.
///
/// A single colour value is 32-bit integer with 8 bits for red,
/// green, blue, and alpha.
///
/// **Memory representation**
///
/// ```text
/// RRRRRRRR GGGGGGGG BBBBBBBB AAAAAAAA
/// ```
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Rgba8888(u32);

impl Rgba8888 {
    #[inline(always)]
    fn make_red(value: u8) -> u32 {
        u32::from(value) << 24
    }

    #[inline(always)]
    fn make_green(value: u8) -> u32 {
        u32::from(value) << 16
    }

    #[inline(always)]
    fn make_blue(value: u8) -> u32 {
        u32::from(value) << 8
    }

    #[inline(always)]
    fn make_alpha(value: u8) -> u32 {
        u32::from(value)
    }

    /// Creates a new colour in `RGBA8888` format using the given `red`,
    /// `green`, `blue`, and `alpha` components.
    #[inline(always)]
    pub fn new(red: u8, green: u8, blue: u8, alpha: u8) -> Self {
        Rgba8888(Self::make_red(red)
                 + Self::make_green(green)
                 + Self::make_blue(blue)
                 + Self::make_alpha(alpha))
    }

    /// Gets red component.
    #[inline(always)]
    pub fn red(self) -> u8 {
        ((self.0 & 0xff00_0000) >> 24) as u8
    }

    /// Gets green component.
    #[inline(always)]
    pub fn green(self) -> u8 {
        ((self.0 & 0x00ff_0000) >> 16) as u8
    }

    /// Gets blue component.
    #[inline(always)]
    pub fn blue(self) -> u8 {
        ((self.0 & 0x0000_ff00) >> 8) as u8
    }

    /// Gets alpha component.
    #[inline(always)]
    pub fn alpha(self) -> u8 {
        (self.0 & 0x0000_00ff) as u8
    }

    /// Changes red component to the given `value`.
    #[inline(always)]
    pub fn set_red(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0x00ff_ff00) + Self::make_red(value);

        self
    }

    /// Changes green component to the given `value`.
    #[inline(always)]
    pub fn set_green(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xff00_ff00) + Self::make_green(value);

        self
    }

    /// Changes blue component to the given `value`.
    #[inline(always)]
    pub fn set_blue(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xffff_0000) + Self::make_blue(value);

        self
    }

    /// Changes alpha component to the given `value`.
    #[inline(always)]
    pub fn set_alpha(&mut self, value: u8) -> &mut Self {
        self.0 = (self.0 & 0xffff_ff00) + Self::make_alpha(value);

        self
    }
}

impl AsRef<u32> for Rgba8888 {
    #[inline(always)]
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

impl AsMut<u32> for Rgba8888 {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut u32 {
        &mut self.0
    }
}

impl From<Rgba8888> for u32 {
    #[inline(always)]
    fn from(colour: Rgba8888) -> u32 {
        colour.0
    }
}
