/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::ops::{Deref, DerefMut};
use std::slice;

use crate::gfx2d::core::Rect;

/// A reference to a [`PixelBuffer`]'s pixels.
///
/// [`PixelBuffer`]: trait.PixelBuffer.html
pub struct PixelRef<'a, T: 'a> {
    pixels:    &'a mut [T],
    clip_rect: Rect,
    width:     usize,
    height:    usize,
    unlock:    Option<Box<dyn Fn()>>,
}

impl<'a, T> PixelRef<'a, T> {
    /// Creates a `PixelRef` from a raw pointer.
    pub unsafe fn from_raw_parts(ptr: *mut T,
                                 width: usize,
                                 height: usize,
                                 clip_rect: Rect,
                                 unlock: Option<Box<dyn Fn()>>)
                                 -> Self {
        assert!(!ptr.is_null());

        let size = width * height;
        let pixels = slice::from_raw_parts_mut(ptr, size);

        PixelRef::from_slice(pixels, width, height, clip_rect, unlock)
    }

    /// Creates a `PixelRef` from a slice of pixels.
    pub fn from_slice(pixels: &'a mut [T],
                      width: usize,
                      height: usize,
                      clip_rect: Rect,
                      unlock: Option<Box<dyn Fn()>>)
                      -> Self {
        assert!(width > 0);
        assert!(height > 0);
        assert_eq!(pixels.len(), width * height);

        assert!(!clip_rect.is_empty());

        assert!(clip_rect.x() >= 0);
        assert!(clip_rect.y() >= 0);
        assert!(clip_rect.right() <= width as isize);
        assert!(clip_rect.bottom() <= height as isize);

        PixelRef { pixels,
                   clip_rect,
                   width,
                   height,
                   unlock }
    }

    /// Width of the buffer in pixels.
    pub fn width(&self) -> usize {
        self.width
    }

    /// Height of the buffer in pixels.
    pub fn height(&self) -> usize {
        self.height
    }

    /// Clipping rectangle.
    ///
    /// A clipping rectangle should be used to make sure no drawing
    /// operation can be done outside of it.
    pub fn clip_rect(&self) -> Rect {
        self.clip_rect
    }
}

impl<'a, T> Drop for PixelRef<'a, T> {
    fn drop(&mut self) {
        if let Some(u) = self.unlock.as_ref() {
            u();
        }
    }
}

impl<'a, T> AsRef<[T]> for PixelRef<'a, T> {
    #[inline(always)]
    fn as_ref(&self) -> &[T] {
        self.pixels
    }
}

impl<'a, T> AsMut<[T]> for PixelRef<'a, T> {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut [T] {
        self.pixels
    }
}

impl<'a, T> Deref for PixelRef<'a, T> {
    type Target = [T];

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        &self.pixels
    }
}

impl<'a, T> DerefMut for PixelRef<'a, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.pixels
    }
}
