/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          05-08-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use crate::gfx2d::core::{PixelRef, Rect};

// TODO: Add clipper.rs with functions that clips items and returns false or ClipResult::{Outside,PartiallyClipped, etc}

/// A pixel buffer represents a linear area of display memory (pixels in the
/// given format).
pub trait PixelBuffer<T> {
    /// Width of the buffer in pixels.
    fn width(&self) -> usize;

    /// Height of the buffer in pixels.
    fn height(&self) -> usize;

    /// Locks the buffer to get access to its pixel data.
    fn lock(&mut self) -> PixelRef<T>;

    fn clip_rect(&self) -> Rect;

    fn set_clip_rect(&mut self, rect: Rect);
}

/// A series of [`PixelBuffer`]s used for smooth, tear-free animation in games
/// and video playback.
///
/// [`PixelBuffer`]: trait.PixelBuffer.html
pub trait SwapChain<T>: PixelBuffer<T> {
    fn swap_buffers(&mut self);
}
