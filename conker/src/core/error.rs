/* SPDX-License-Identifier: Apache-2.0 */

/*
 * Created:          10-10-2018
 * Original Authors: Oleksandr Manenko <oleksandr@manenko.com>
 *
 *
 * Copyright 2018 Oleksandr Manenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use std::error;
use std::fmt;

pub struct ArgumentError<T> {
    kind: T,
}

impl<T> ArgumentError<T> {
    pub fn new(kind: T) -> Self {
        ArgumentError { kind }
    }

    pub fn kind(&self) -> &T {
        &self.kind
    }
}

impl<T: fmt::Debug> fmt::Debug for ArgumentError<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&self.kind, f)
    }
}

impl<T: fmt::Display> fmt::Display for ArgumentError<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.kind, f)
    }
}

impl<T: error::Error> error::Error for ArgumentError<T> {
    fn cause(&self) -> Option<&dyn error::Error> {
        Some(&self.kind)
    }
}
